window.onload = function() {
    deactivateNotSelected();
    document.getElementById("fromkeyboard").addEventListener("click", function() {
        activateElement("inpTextDNA");
        deactivateElement("inpFileDNA");
    });
    document.getElementById("fromfile").addEventListener("click", function() {
        activateElement("inpFileDNA");
        deactivateElement("inpTextDNA");
    });
    document.getElementById("stype-kSimilar").addEventListener("click", function() {
        activateElement("numberofoutputs");
        deactivateElement("threshold");
    });
    document.getElementById("stype-threshold").addEventListener("click", function() {
        activateElement("threshold");
        deactivateElement("numberofoutputs");
    });
};

function activateElement(elemName) {
    document.getElementById(elemName).removeAttribute("disabled");
}

function deactivateElement(elemName) {
    document.getElementById(elemName).setAttribute("disabled", "disabled");
}

function deactivateNotSelected() {
    if(document.getElementById("fromkeyboard").checked) {
        deactivateElement("inpFileDNA");
    }
    if(document.getElementById("fromfile").checked) {
        deactivateElement("inpTextDNA");
    }
    if(document.getElementById("stype-kSimilar").checked) {
        deactivateElement("threshold");
    }
    if(document.getElementById("stype-threshold").checked) {
        deactivateElement("numberofoutputs");
    }
}