

package edu.cvut.fit.kw.vmm.backend.sequence_containers;

import edu.cvut.fit.kw.vmm.DnaSequence;

/**
 * Contains all reference DNA sequences and can iterate over them. It also limits length of the sequences to the
 * specified length. Everything longer will be trimmed Default lenght of sequence is unlimited.
 */
public interface SequenceContainer extends Iterable<DnaSequence> {

}
