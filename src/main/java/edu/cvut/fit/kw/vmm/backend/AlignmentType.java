package edu.cvut.fit.kw.vmm.backend;

public enum AlignmentType {
    LOCAL, GLOBAL
}
