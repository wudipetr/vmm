package edu.cvut.fit.kw.vmm.frontend;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileLoader {

    /**
     * Load file from specified path
     * @param path file to load from
     * @return content of the page as byte array
     */
    public static byte[] loadPage(String path) {
        try {
            return Files.readAllBytes(Paths.get(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new byte[0];
    }

    public static String loadPageStr(String path) {
        return new String(loadPage(path));
    }
}
