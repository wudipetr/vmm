package edu.cvut.fit.kw.vmm;

/**
 * DNA sequence
 */
public class DnaSequence {

    protected String animalName;
    protected String sequence;

    public DnaSequence(String animalName, String sequence) {
        this.animalName = animalName;
        this.sequence = sequence;
    }

    /**
     * @return name of animal whose DNA sequence this is
     */
    public String getAnimalName() {
        return animalName;
    }

    /**
     * @return the actual DNA sequence
     */
    public String getSequence() {
        return sequence;
    }
}
