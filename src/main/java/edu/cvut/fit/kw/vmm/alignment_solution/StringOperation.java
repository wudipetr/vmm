package edu.cvut.fit.kw.vmm.alignment_solution;

/**
 * Class representing string modification. It describes an operation which can be done with one string to get another one.
 */
public enum StringOperation {
    /**
     * Copy char from the first sequence to the second one (the characters at this position are equal)
     */
    COPY,

    /**
     * Replace character in the original string with character in the second string
     */
    REPLACE,

    /**
     * Add a character from the second string
     */
    ADD,

    /**
     * Drop a character
     */
    DELETE
}