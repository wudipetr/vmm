package edu.cvut.fit.kw.vmm;

import edu.cvut.fit.kw.vmm.alignment_solution.AlignmentSolution;
import edu.cvut.fit.kw.vmm.alignment_solution.StringOperation;
import edu.cvut.fit.kw.vmm.backend.SimilarDnaFinder;
import edu.cvut.fit.kw.vmm.backend.SimilarDnaFinderImpl;
import edu.cvut.fit.kw.vmm.backend.sequence_containers.FileSequenceContainer;
import edu.cvut.fit.kw.vmm.backend.sequence_containers.SequenceContainer;
import edu.cvut.fit.kw.vmm.backend.sequences_comparement.NeedlemanWunsch;
import edu.cvut.fit.kw.vmm.backend.sequences_comparement.SmithWaterman;

import edu.cvut.fit.kw.vmm.frontend.OutputPageHandler;


import com.sun.net.httpserver.HttpServer;
import edu.cvut.fit.kw.vmm.frontend.ResourceHandler;

import java.io.IOException;
import java.net.InetSocketAddress;


import java.io.File;
import java.util.List;

public class Main {

    private static final String DEFAULT_CONTAINER_PATH = "." + File.separator + "data";
    private static final int SERVER_PORT_DEFAULT = 8080;
    private static final int SERVER_BACKLOG_DEFAULT = 10;

    public static void main(String[] args) {
        String containerPath = DEFAULT_CONTAINER_PATH;
        SequenceContainer container = new FileSequenceContainer(containerPath);
        SimilarDnaFinder similarDnaFinder = new SimilarDnaFinderImpl(container);
        try {
            HttpServer server = HttpServer.create(new InetSocketAddress(SERVER_PORT_DEFAULT), SERVER_BACKLOG_DEFAULT);
            server.createContext("/inputs", new ResourceHandler("./html/inputs.html"));
            server.createContext("/outputs/", new OutputPageHandler(similarDnaFinder));
            server.createContext("/css/style.css", new ResourceHandler("./html/css/style.css"));
            server.createContext("/css/skeleton.css", new ResourceHandler("./html/css/skeleton.css"));
            server.createContext("/js/script.js", new ResourceHandler("./html/js/script.js"));
            server.setExecutor(null); // creates a default executor
            System.out.println("Server is listening on port " + SERVER_PORT_DEFAULT + ".");
            server.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //needleTest("ABC", "TAC");
        //needleTest("CATAC", "ATCGAC");
        //needleTest("I like trains!", "I like BulĂˇnci sleeping!");
        //needleTest("I like pizza!", "Ich mag nudeln!");
        //needleTest("TRALALA", "Ich mag nudeln!");
    	//smithTest("CATAC", "ATCGAA");
    	//smithTest("GGTTGACTA","TGTTACGG");
    }

    private static void needleTest(String str0, String str1) {
        System.out.println(str0 + " --- " + str1);
        NeedlemanWunsch needle = new NeedlemanWunsch(str0, str1);
        AlignmentSolution solution = needle.solve();
        needle.printMatrices();
        printSolution(solution, str0, str1);
        System.out.println(solution);
        System.out.println("");
    }
    
    private static void smithTest(String str0, String str1) {
    	System.out.println("SMITH:" + str0 + " --- " + str1);
    	SmithWaterman smith = new SmithWaterman(str0, str1);
    	AlignmentSolution solution = smith.solve();
    	smith.printMatrices();
    	printSolution(solution, str0, str1);
    	System.out.println(solution);
    	System.out.println("");
    }

    /**
     * Example how can printing the solution look like
     * @param solution solved solution - how to transform the first string into the second one
     * @param str0 first string
     * @param str1 second string
     */
    private static void printSolution(AlignmentSolution solution, String str0, String str1) {
        List<StringOperation> operations = solution.getOperations();
        int str0Pointer = solution.getStr0Beginning();
        int str1Pointer = solution.getStr1Beginning();
        String res0 = "";
        String res1 = "";
        String verticalLines = "";
        System.out.println("====== Printing solution: ======");
        System.out.println(str0);
        System.out.println(str1);
        // PRINT THE PART BEFORE ALIGNED SEQUENCE
        for(int i = 0; i < Integer.min(str0Pointer, str1Pointer); i++) {
            res0 += str0.charAt(i);
            res1 += str1.charAt(i);
            verticalLines += "►";
        }
        for(int i = Integer.min(str0Pointer, str1Pointer); i < str0Pointer; i++) {
            res0 += str0.charAt(i);
            res1 += "-";
            verticalLines += "►";
        }
        for(int i = Integer.min(str0Pointer, str1Pointer); i < str1Pointer; i++) {
            res0 += "-";
            res1 += str1.charAt(i);
            verticalLines += "►";
        }
        // ACTUAL ALIGNED SEQUENCE
        for(StringOperation operation:operations) {
            switch (operation) {
                case COPY:
                    res0 += str0.charAt(str0Pointer);
                    res1 += str1.charAt(str1Pointer);
                    verticalLines += "|";
                    str0Pointer++;
                    str1Pointer++;
                    break;
                case REPLACE:
                    res0 += str0.charAt(str0Pointer);
                    res1 += str1.charAt(str1Pointer);
                    verticalLines += " ";
                    str0Pointer++;
                    str1Pointer++;
                    break;
                case ADD:
                    res0 += "-";
                    res1 += str1.charAt(str1Pointer);
                    verticalLines += " ";
                    str1Pointer++;
                    break;
                case DELETE:
                    res0 += str0.charAt(str0Pointer);
                    res1 += "-";
                    verticalLines += " ";
                    str0Pointer++;
            }
        }
        // PRINT THE PART AFTER ALIGNED SEQUENCE
        while(str0Pointer < str0.length() && str1Pointer < str1.length()) {
            res0 += str0.charAt(str0Pointer);
            res1 += str1.charAt(str1Pointer);
            verticalLines += "►";
            str0Pointer++;
            str1Pointer++;
        }
        while(str0Pointer < str0.length()) {
            res0 += str0.charAt(str0Pointer);
            res1 += "-";
            verticalLines += "►";
            str0Pointer++;
        }
        while(str1Pointer < str1.length()) {
            res0 += "-";
            res1 += str1.charAt(str1Pointer);
            verticalLines += "►";
            str1Pointer++;
        }
        // PRINT IT TO STDOUT
        System.out.println(res0);
        System.out.println(verticalLines);
        System.out.println(res1);
    }

}
