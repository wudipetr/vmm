package edu.cvut.fit.kw.vmm.frontend;

import java.io.*;
import java.util.*;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import edu.cvut.fit.kw.vmm.alignment_solution.AlignedSequencePair;
import edu.cvut.fit.kw.vmm.alignment_solution.AlignmentSolution;
import edu.cvut.fit.kw.vmm.alignment_solution.StringOperation;
import edu.cvut.fit.kw.vmm.backend.AlignmentType;
import edu.cvut.fit.kw.vmm.backend.SimilarDnaFinder;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.RequestContext;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

public class OutputPageHandler implements HttpHandler {
    private final SimilarDnaFinder similarDnaFinder;
    protected String response;

    protected AlignmentType alignmentType;
    protected String inpFileDNA;
    protected String inpTextDNA;
    protected int numberOfOutputs;
    protected int threshold;
    protected boolean inputSequenceIsFromFile;
    protected boolean similarityTypeIsKSimilar;

    public OutputPageHandler(SimilarDnaFinder similarDnaFinder) {
        this.similarDnaFinder = similarDnaFinder;
        inpFileDNA = null;
        inpTextDNA = null;
        inputSequenceIsFromFile = false;
    }

    public synchronized void handle(HttpExchange exchange) throws IOException {
		System.out.println("OutputPage request.");
		response = "";
		List<FileItem> params = parseRequest(exchange);
		if(params != null) {
		    for(FileItem fi : params) {
                String paramName = fi.getFieldName();
                String value = fi.getString();
                setParam(paramName, value);
            }
            if(validateParams()) {
                List<AlignedSequencePair> foundSequences = computeSolution();
                printSolutionList(foundSequences);
            }
            exchange.sendResponseHeaders(200, response.getBytes().length);
            OutputStream os = exchange.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
        else {
            exchange.sendResponseHeaders(404, response.getBytes().length);
        }
    }

    /**
     * Sets specified parameter's variable
     * @param paramName identifier of the parameter
     * @param value variable's new value
     */
    private void setParam(String paramName, String value) {
        if (paramName.compareTo("alignmentType") == 0) {
            if (value.compareTo("needlemanwunsch") == 0) {
                alignmentType = AlignmentType.GLOBAL;
            } else if (value.compareTo("smithwaterman") == 0) {
                alignmentType = AlignmentType.LOCAL;
            }
        }
        else if (paramName.compareTo("inpFileDNA") == 0) {
            inpFileDNA = value;
        }
        else if (paramName.compareTo("inpTextDNA") == 0) {
            inpTextDNA = value;
        }
        else if (paramName.compareTo("numberofoutputs") == 0) {
            numberOfOutputs = Integer.parseInt(value);
        }
        else if (paramName.compareTo("threshold") == 0) {
            threshold = Integer.parseInt(value);
        }
        else if (paramName.compareTo("inpSequence") == 0) {
            if (value.compareTo("fromkeyboard") == 0) {
                inputSequenceIsFromFile = false;
            } else if (value.compareTo("fromfile") == 0) {
                inputSequenceIsFromFile = true;
            }
        }
        else if (paramName.compareTo("similarityType") == 0) {
            if (value.compareTo("kSimilar") == 0) {
                similarityTypeIsKSimilar = true;
            } else if (value.compareTo("threshold") == 0) {
                similarityTypeIsKSimilar = false;
            }
        }
    }

    /**
     * Parses request params (values sent by POST method) using encoding 'multipart/form-data'
     * @param exchange exchange object containing the parameters
     * @return list of structures containing param name and the value (file content in case of file)
     */
    private List<FileItem> parseRequest(HttpExchange exchange) {
        DiskFileItemFactory d = new DiskFileItemFactory();
        List<FileItem> result = null;
        try {
            ServletFileUpload up = new ServletFileUpload(d);
            result = up.parseRequest(new RequestContext() {
                @Override
                public int getContentLength() {
                    return 0;
                }

                @Override
                public String getCharacterEncoding() {
                    return "UTF-8";
                }

                @Override
                public String getContentType() {
                    String ct = exchange.getRequestHeaders().getFirst("Content-type");
                    return ct == null ? "text/plain" : ct;
                }

                @Override
                public InputStream getInputStream() throws IOException {
                    return exchange.getRequestBody();
                }
            });

        } catch (Exception e) {
            return null;
        }
        return result;
    }

    /**
     * Checks for validity of input params. It prints the error message (including HTML tags) to the 'result' string.
     * @return true if all params are valid, false if some isn't
     */
    private boolean validateParams() {
        if (inputSequenceIsFromFile && (inpFileDNA == null || inpFileDNA.length() <= 0)) {
            response += FileLoader.loadPageStr(FrontendConstants.ERROR_NOT_SPECIFIED_FILE);
            return false;
        } else if (!inputSequenceIsFromFile && (inpTextDNA == null || inpTextDNA.length() <= 0)) {
            response += FileLoader.loadPageStr(FrontendConstants.ERROR_NOT_SPECIFIED_SEQUENCE);
            return false;
        }
        return true;
    }

    /**
     * Computes solution of alignment problem. All inputs and parameters of the problem must be already stored in this
     * object's variables.
     * @return list of selected solution (number of them depends on the params)
     */
    private List<AlignedSequencePair> computeSolution() {
        String inputSeq = inputSequenceIsFromFile ? inpFileDNA : inpTextDNA;
        List<AlignedSequencePair> foundSequences;
        if(similarityTypeIsKSimilar) {
            foundSequences = similarDnaFinder.findKSimilar(inputSeq, alignmentType, numberOfOutputs);
        }
        else {
            foundSequences = similarDnaFinder.findSimilarWithRange(inputSeq, alignmentType, threshold);
        }
        return foundSequences;
    }

    /**
     * Prints list of found sequences into 'response' field.
     * @param foundSequences list of sequences to be print
     */
    private void printSolutionList(List<AlignedSequencePair> foundSequences) {
        if(foundSequences.size() == 0) {
            response += FileLoader.loadPageStr(FrontendConstants.OUTPUT_PAGE_EMPTY);
        }
        else {
            response += "<html><head><meta http-equiv=\"content-type\" content=\"charset=UTF-8\">" +
                    "<link rel=\"stylesheet\" href=\"../css/style.css\"></head><body>";
            foundSequences.sort(Comparator.comparing(AlignedSequencePair::getAlignment));
            for (int i = foundSequences.size() - 1; i >= 0; i--) {
                AlignedSequencePair pair = foundSequences.get(i);
                response += "<div class=\"record\">";
                response += "<div class=\"id\">" + (foundSequences.size() - i) + "</div>";
                response += "<div class=\"animal-name\">" + pair.getSecondSequence().getAnimalName() + "</div>";
                response += "<div class=\"similarity\">Podobnost: " + pair.getAlignment().getSimilarity() + "</div>";
                System.out.println("Animal: " + pair.getSecondSequence().getAnimalName());
                printSolution(pair.getAlignment(), pair.getFirstSequence().getSequence(), pair.getSecondSequence().getSequence());
                System.out.println(pair.getAlignment().getSimilarity());
                response += "</div>";
            }
            response += "</body></html>";
        }
    }

    /**
     * Example how can printing the solution look like
     * @param solution solved solution - how to transform the first string into the second one
     * @param str0 first string
     * @param str1 second string
     */
    private void printSolution(AlignmentSolution solution, String str0, String str1) {
        List<StringOperation> operations = solution.getOperations();
        int str0Pointer = solution.getStr0Beginning();
        int str1Pointer = solution.getStr1Beginning();
        String res0 = "";
        String res1 = "";
        String verticalLines = "";
        // PRINT THE PART BEFORE ALIGNED SEQUENCE
        for(int i = 0; i < Integer.min(str0Pointer, str1Pointer); i++) {
            res0 = addCharCell(res0, str0.charAt(i), "top-before");
            verticalLines = addCharCell(verticalLines, ' ', "middle-before");
            res1 = addCharCell(res1, str1.charAt(i), "bottom-before");
        }
        for(int i = Integer.min(str0Pointer, str1Pointer); i < str0Pointer; i++) {
            res0 = addCharCell(res0, str0.charAt(i), "top-before");
            verticalLines = addCharCell(verticalLines, ' ', "middle-before");
            res1 = addCharCell(res1, '-', "bottom-before");
        }
        for(int i = Integer.min(str0Pointer, str1Pointer); i < str1Pointer; i++) {
            res0 = addCharCell(res0, '-', "top-before");
            verticalLines = addCharCell(verticalLines, ' ', "middle-before");
            res1 = addCharCell(res1, str1.charAt(i), "bottom-before");
        }
        // ACTUAL ALIGNED SEQUENCE
        for(StringOperation operation:operations) {
            switch (operation) {
                case COPY:
                    res0 = addCharCell(res0, str0.charAt(str0Pointer), "top-match");
                    verticalLines = addCharCell(verticalLines, '|', "middle-match");
                    res1 = addCharCell(res1, str1.charAt(str1Pointer), "bottom-match");
                    str0Pointer++;
                    str1Pointer++;
                    break;
                case REPLACE:
                    res0 = addCharCell(res0, str0.charAt(str0Pointer), "top-regular");
                    verticalLines = addCharCell(verticalLines, ' ', "middle-regular");
                    res1 = addCharCell(res1, str1.charAt(str1Pointer), "bottom-regular");
                    str0Pointer++;
                    str1Pointer++;
                    break;
                case ADD:
                    res0 = addCharCell(res0, '-', "top-regular");
                    verticalLines = addCharCell(verticalLines, ' ', "middle-regular");
                    res1 = addCharCell(res1, str1.charAt(str1Pointer), "bottom-regular");
                    str1Pointer++;
                    break;
                case DELETE:
                    res0 = addCharCell(res0, str0.charAt(str0Pointer), "top-regular");
                    verticalLines = addCharCell(verticalLines, ' ', "middle-regular");
                    res1 = addCharCell(res1, '-', "bottom-regular");
                    str0Pointer++;
            }
        }
        // PRINT THE PART AFTER ALIGNED SEQUENCE
        while(str0Pointer < str0.length() && str1Pointer < str1.length()) {
            res0 = addCharCell(res0, str0.charAt(str0Pointer), "top-after");
            verticalLines = addCharCell(verticalLines, ' ', "middle-after");
            res1 = addCharCell(res1, str1.charAt(str1Pointer), "bottom-after");
            str0Pointer++;
            str1Pointer++;
        }
        while(str0Pointer < str0.length()) {
            res0 = addCharCell(res0, str0.charAt(str0Pointer), "top-after");
            verticalLines = addCharCell(verticalLines, ' ', "middle-after");
            res1 = addCharCell(res1, '-', "bottom-after");
            str0Pointer++;
        }
        while(str1Pointer < str1.length()) {
            res0 = addCharCell(res0, '-', "top-after");
            verticalLines = addCharCell(verticalLines, ' ', "middle-after");
            res1 = addCharCell(res1, str1.charAt(str1Pointer), "bottom-after");
            str1Pointer++;
        }
        // PRINT IT TO THE STRING
        response += "<div class=\"outputs\"><table><tr>";
        response += res0;
        response += "<tr></tr>";
        response += verticalLines;
        response += "<tr></tr>";
        response += res1;
        response += "</tr></table></div>";
    }

    /**
     * Appends HTML code of table cell containing specified char to the specified string.
     * @param s string where HTML code will be appended
     * @param c char in the table cell
     * @param className CSS class of the cell
     * @return the resulting string
     */
    private String addCharCell(String s, char c, String className) {
        return s + "<td class=\"" + className + "\">" + c + "</td>";
    }
}