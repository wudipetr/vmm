package edu.cvut.fit.kw.vmm.backend;

import edu.cvut.fit.kw.vmm.DnaSequence;
import edu.cvut.fit.kw.vmm.ScoringMatrix;
import edu.cvut.fit.kw.vmm.alignment_solution.AlignedSequencePair;
import edu.cvut.fit.kw.vmm.alignment_solution.AlignmentSolution;
import edu.cvut.fit.kw.vmm.backend.sequences_comparement.NeedlemanWunsch;
import edu.cvut.fit.kw.vmm.backend.sequences_comparement.SmithWaterman;
import edu.cvut.fit.kw.vmm.backend.sequences_comparement.SequenceComparator;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import edu.cvut.fit.kw.vmm.backend.sequence_containers.SequenceContainer;

/**
 * Finds similar DNA sequences in specified container
 */
public class SimilarDnaFinderImpl implements SimilarDnaFinder {

    private static final String INPUT_SEQUENCE_NAME = "input";
    private final SequenceContainer container;
    private ScoringMatrix scoringMatrix;

    /**
     * Creates new instance of class for finding similar sequences to the specified one in the sequence container.
     * @param container object containing all the sequences which will be compared to the specified one
     */
    public SimilarDnaFinderImpl(SequenceContainer container) {
        this.container = container;
        this.scoringMatrix = new ScoringMatrix();
    }

    @Override
    public List<AlignedSequencePair> findKSimilar(String inputSeqString, AlignmentType alignmentType, int k) {
        MostSimilarSequenceList similarSequences = new KMostSimilarSequencesList(k);
        return findSimilar(inputSeqString, alignmentType, similarSequences);
    }

    @Override
    public List<AlignedSequencePair> findSimilarWithRange(String inputSeqString, AlignmentType alignmentType, double minSimilarity) {
        MostSimilarSequenceList similarSequences = new ThresholdSequencesList(minSimilarity);
        return findSimilar(inputSeqString, alignmentType, similarSequences);
    }

    /**
     * Finds DNA sequences most similar to the specified one and way how they should be aligned
     * @param inputSeqString sequences similar to this one will be found
     * @param alignmentType the way how to align the sequences
     * @param similarSequences list of where all found sequences will be stored in (it automatically accepts or rejects
     *                         the sequences according to their similarity score)
     * @return list of found DNA sequences with the best way to align them (align to the input sequence)
     */
    private List<AlignedSequencePair> findSimilar(String inputSeqString, AlignmentType alignmentType, MostSimilarSequenceList similarSequences) {
        DnaSequence inputSequence = new DnaSequence(INPUT_SEQUENCE_NAME, inputSeqString);
        for(DnaSequence sequence : container) {
            String seqString = sequence.getSequence();
            SequenceComparator comparator = null;
            if(alignmentType == AlignmentType.GLOBAL) {
                comparator = new NeedlemanWunsch(inputSeqString, seqString, scoringMatrix);
                AlignmentSolution alignment = comparator.solve();
                similarSequences.tryAdd(inputSequence, sequence, alignment);
            } else if (alignmentType == AlignmentType.LOCAL) { 
            	comparator = new SmithWaterman(inputSeqString, seqString, scoringMatrix);
            	AlignmentSolution alignment = comparator.solve();
                similarSequences.tryAdd(inputSequence, sequence, alignment);
            }
        }
        return similarSequences.getList();
    }

    @Override
    public void setScoringMatrix(ScoringMatrix scoringMatrix) {
        this.scoringMatrix = scoringMatrix;
    }
    
    /**
     * List of DNA sequences most similar to the original one.
     */
    protected interface MostSimilarSequenceList {
        /**
         * Tries add specified sequence to the list
         * @param seq0 first sequence (the one beying part of the input)
         * @param seq1 second sequence (the one found)
         * @param alignment best way to align the sequences
         */
        void tryAdd(DnaSequence seq0, DnaSequence seq1, AlignmentSolution alignment);

        /**
         * @return the list of sequences and their alignment in format of Java list
         */
        List<AlignedSequencePair> getList();
    }

    /**
     * List of k sequences most similar to the original one.
     */
    protected class KMostSimilarSequencesList implements MostSimilarSequenceList {
        int maxSize;
        List<AlignedSequencePair> similarSequences;

        public KMostSimilarSequencesList(int k) {
            this.maxSize = k;
            similarSequences = new LinkedList<>();
        }
        @Override
        public void tryAdd(DnaSequence seq0, DnaSequence seq1, AlignmentSolution alignment) {
            int firstHigherScoreValue = 0; // Index of first sequence in the list which has score higher than this one
            while(firstHigherScoreValue < similarSequences.size() &&
                    similarSequences.get(firstHigherScoreValue).getAlignment().getSimilarity() < alignment.getSimilarity()) {
                firstHigherScoreValue++;
            }
            if(firstHigherScoreValue > 0 || similarSequences.size() < maxSize) {
                similarSequences.add(firstHigherScoreValue, new AlignedSequencePair(seq0, seq1, alignment));
                if(similarSequences.size() > maxSize) {
                    similarSequences.remove(0);
                }
            }

        }
        @Override
        public List<AlignedSequencePair> getList() {
            return similarSequences;
        }
    }


    /**
     * List sequences that are similar to the original one. It contains only sequences with specified similarity score
     * equal to specified argument or better.
     */
    protected class ThresholdSequencesList implements MostSimilarSequenceList {
        double minSimilarity;
        List<AlignedSequencePair> similarSequences;

        public ThresholdSequencesList(double minSimilarity) {
            this.minSimilarity = minSimilarity;
            similarSequences = new ArrayList<>();
        }
        @Override
        public void tryAdd(DnaSequence seq0, DnaSequence seq1, AlignmentSolution alignment) {
            if(alignment.getSimilarity() >= minSimilarity) {
                similarSequences.add(new AlignedSequencePair(seq0, seq1, alignment));
            }
        }
        @Override
        public List<AlignedSequencePair> getList() {
            return similarSequences;
        }
    }
}
