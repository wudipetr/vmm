package edu.cvut.fit.kw.vmm;

/**
 * Creates new scoring matrix. Default scoring matrix contains 1 on the diagonal and -1 everywhere else. Score
 * (similarity) of two symbols can be set by a method or as matrix during matrix initialization.
 */
public class ScoringMatrix {

    private static final char[] indexes = {'a', 'c', 'g', 't'};
    int[][] scoringMatrix;

    /**
     * Creates new matrix with score described by input parameter matrix.
     * @param scoringMatrix matrix containing relations between these four characters: a, c, g, t (in this order).
     */
    public ScoringMatrix(int[][] scoringMatrix) {
        this.scoringMatrix = scoringMatrix;
    }

    /**
     * Creates default scoring matrix
     */
    public ScoringMatrix() {
        scoringMatrix = new int[indexes.length][indexes.length];
        for(int i = 0; i < indexes.length; i++) {
            for(int j = 0; j < indexes.length; j++) {
                scoringMatrix[i][j] = -3;
            }
            scoringMatrix[i][i] = 3;
        }
    }

    /**
     * Sets element of scoring matrix. Score between c0 and c1 (and also between c1 and c0, it's symmetric) is set to
     * the specified value.
     * @param c0 first char of the relation we set the score
     * @param c1 second char of the relation we set the score
     * @param score the score we set
     */
    public void setScore(char c0, char c1, int score) {
        int i0 = getIndex(c0);
        int i1 = getIndex(c1);
        if(i0 < 0 || i1 < 0) {
            throw new IllegalArgumentException("Invalid index in scoring matrix");
        }
        scoringMatrix[i0][i1] = score;
        scoringMatrix[i1][i0] = score;
    }

    public int getScore(char c0, char c1) {
        int i0 = getIndex(c0);
        int i1 = getIndex(c1);
        if(i0 < 0 || i1 < 0) {
            throw new IllegalArgumentException("Invalid index in scoring matrix");
        }
        return scoringMatrix[i0][i1];
    }

    /**
     * Get index of a char in scoring matrix
     * @param c the char whose index is searched
     * @return index of the char or -1
     */
    private int getIndex(char c) {
        for(int i = 0; i < indexes.length; i++) {
            char indexChar = indexes[i];
            if(c == indexChar) {
                return i;
            }
        }
        return -1;
    }

}
