package edu.cvut.fit.kw.vmm.alignment_solution;

import edu.cvut.fit.kw.vmm.DnaSequence;

/**
 * Contains pair of sequences and information how to align them to have mathing part of the sequences on the same
 * position.
 */
public class AlignedSequencePair {

    private final AlignmentSolution alignment;
    private final DnaSequence seq0;
    private final DnaSequence seq1;

    public AlignedSequencePair(DnaSequence seq0, DnaSequence seq1, AlignmentSolution alignment) {
        this.seq0 = seq0;
        this.seq1 = seq1;
        this.alignment = alignment;
    }

    /**
     * @return information how to align them to have mathing part of the sequences on the same position. It specifies
     * how to transform first string into the second one.
     */
    public AlignmentSolution getAlignment() {
        return alignment;
    }

    /**
     * @return first DNA sequence
     */
    public DnaSequence getFirstSequence() {
        return seq0;
    }

    /**
     * @return second DNA sequence
     */
    public DnaSequence getSecondSequence() {
        return seq1;
    }
}
