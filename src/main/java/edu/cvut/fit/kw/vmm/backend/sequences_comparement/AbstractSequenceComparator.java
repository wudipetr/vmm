package edu.cvut.fit.kw.vmm.backend.sequences_comparement;

import edu.cvut.fit.kw.vmm.ScoringMatrix;
import edu.cvut.fit.kw.vmm.alignment_solution.AlignmentSolution;

import java.io.PrintStream;

/**
 * Contains methods and variables useful for both Needleman-Wunsch and Smith-Waterman algorithms
 */
abstract class AbstractSequenceComparator implements SequenceComparator {

    protected ScoringMatrix charSimilarity;
    protected static final int DISTANCE_PENALTY = -2;
    protected int[][] scores;
    protected int[][] bBScore;
    protected NeedlemanWunsch.TableDirection[][] directions;
    protected String str0;
    protected String str1;
    protected int bestScoreX;
    protected int bestScoreY;

    protected AbstractSequenceComparator(String str0, String str1) {
        this.str0 = str0;
        this.str1 = str1;
        charSimilarity = new ScoringMatrix();
    }

    /**
     * Creates new sequence comparator with specified char similarity score
     * @param str0 first string to compare
     * @param str1 second string to compare
     * @param charSimilarity matrix specifying how are chars similar
     */
    protected AbstractSequenceComparator(String str0, String str1, ScoringMatrix charSimilarity) {
        this.str0 = str0;
        this.str1 = str1;
        this.charSimilarity = charSimilarity;
    }

    /**
     * Initializes matrix of partial similarity scores.
     * Allocates it and fills first row and column.
     */
    protected abstract void initScoreMatrix();

    /**
     * Initializes directions of steps in score matrix
     * Allocates it and fills first row and column.
     */
    protected void initDirectionMatrix() {
        directions = new NeedlemanWunsch.TableDirection[str0.length() + 1][str1.length() + 1];
        directions[0][0] = null;
        for(int i = 1; i <= str0.length(); i++) {
            directions[i][0] = NeedlemanWunsch.TableDirection.VERTICAL;
        }
        for(int i = 1; i <= str1.length(); i++) {
            directions[0][i] = NeedlemanWunsch.TableDirection.HORIZONTAL;
        }
    }

    /**
     * Generates Alignment solution instance from the solution stored in the matrices.
     * @return Alignment solution of the two strings (those which are set up during class construction)
     */
    protected abstract AlignmentSolution getAlignmentSolution();

    /**
     * Prints score and direction matrices to stdout (useful for debugging)
     */
    public void printMatrices() {
        printScoreMatrix();
        printDirectionMatrix();
    }

    /**
     * Prints score matrix to stdout (useful for debugging)
     */
    public void printScoreMatrix() {
        PrintStream stream = System.out;
        // Print header (first string)
        stream.format("     ");
        for(int i = 0; i < str1.length(); i++) {
            stream.format("%5c", str1.charAt(i));
        }
        stream.println();
        // Print actual matrix
        for(int y = 0; y <= str0.length(); y++) {
            if(y != 0) {
                stream.format("%c", str0.charAt(y - 1));
            }
            else {
                stream.format(" ");
            }
            for(int x = 0; x <= str1.length(); x++) {
                stream.format("%4d ", scores[y][x]);
            }
            stream.println();
        }
    }

    /**
     * Prints score matrix to stdout (useful for debugging)
     */
    public void printDirectionMatrix() {
        PrintStream stream = System.out;
        // Print header (first string)
        stream.format("   ");
        for(int i = 0; i < str1.length(); i++) {
            stream.format("%c ", str1.charAt(i));
        }
        stream.println();
        // Print actual matrix
        for(int y = 0; y <= str0.length(); y++) {
            if(y != 0) {
                stream.format("%c ", str0.charAt(y - 1));
            }
            else {
                stream.format("  ");
            }
            for(int x = 0; x <= str1.length(); x++) {
                NeedlemanWunsch.TableDirection direction = directions[y][x];
                if (direction == null) {
                    stream.format(" ");
                }
                else{
                    switch (direction) {
                        case DIAGONAL:
                            stream.format("\\ ");
                            break;
                        case VERTICAL:
                            stream.format("| ");
                            break;
                        case HORIZONTAL:
                            stream.format("- ");
                            break;
                    }
                }
            }
            stream.println();
        }
    }

    /**
     * Returns similarity score between two specified characters
     * @param a first character to compare
     * @param b another character to compare
     * @return similarity - positive or negative value or zero
     */
    protected int getSimilarity(char a, char b) {
        // Just for test:
        a = Character.toLowerCase(a);
        b = Character.toLowerCase(b);
        if((a != 'a' && a != 'c' && a != 'g' && a != 't') || (b != 'a' && b != 'c' && b != 'g' && b != 't')) {
            return a == b ? 1 : -1;
        }
        // this is real:
        return charSimilarity.getScore(Character.toLowerCase(a), Character.toLowerCase(b));
    }

    protected enum TableDirection {
        HORIZONTAL, VERTICAL, DIAGONAL, STOP
    }

}
