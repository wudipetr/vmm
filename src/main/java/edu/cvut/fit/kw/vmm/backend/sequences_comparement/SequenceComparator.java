package edu.cvut.fit.kw.vmm.backend.sequences_comparement;

import edu.cvut.fit.kw.vmm.alignment_solution.AlignmentSolution;

/**
 * Compares sequences and aligns them. The sequences to compare are passed as constructor parameter or any another way
 * (it depends on the actual class).
 */
public interface SequenceComparator {

    AlignmentSolution solve();

}
