package edu.cvut.fit.kw.vmm.frontend;

import java.io.File;

public class FrontendConstants {
    public static final String HTML_DIR = "." + File.separator + "html";
    public static final String OUTPUT_PAGE_EMPTY = HTML_DIR + File.separator + "output-page-empty.html";
    public static final String ERROR_NOT_SPECIFIED_FILE = HTML_DIR + File.separator + "err-not-specified-file.html";
    public static final String ERROR_NOT_SPECIFIED_SEQUENCE = HTML_DIR + File.separator + "err-not-specified-sequence.html";

}
