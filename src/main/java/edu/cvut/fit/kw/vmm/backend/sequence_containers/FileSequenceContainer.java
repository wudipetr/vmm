package edu.cvut.fit.kw.vmm.backend.sequence_containers;

import edu.cvut.fit.kw.vmm.DnaSequence;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import edu.cvut.fit.kw.vmm.backend.sequence_containers.SequenceContainer;

/**
 * Contains all reference DNA sequences loaded from files
 */
public class FileSequenceContainer implements SequenceContainer {

    private static final String ERR_LOAD_SEQ_FILE = "Can't load input sequence file.";

    int seqMaxLength;
    List<File> files;

    /**
     * Creates container of sequences. These sequences are loaded from specified file.
     * @param inputFilePath path of file containing DNA sequences. If path points to a directory, it recursively finds
     *                   all files in it and load as DNS sequence files.
     */
    public FileSequenceContainer(String inputFilePath) {
        this(new ArrayList<>());
        File file = new File(inputFilePath);
        loadFileOrDirectory(file);
    }

    /**
     * Creates container of sequences. These sequences are loaded from specified files.
     * @param inputFilePaths paths of files containing DNA sequences. If path points to a directory, it recursively finds
     *                   all files in it and load as DNS sequence files.
     */
    public FileSequenceContainer(List<String> inputFilePaths) {
        seqMaxLength = -1;
        files = new ArrayList<>();
        for(String filePath : inputFilePaths) {
            File file = new File(filePath);
            loadFileOrDirectory(file);
        }
    }

    /**
     * Adds file to the file list if it's regular file or recursively load it's content if it's directory
     * @param file the file or directory this is all about
     */
    private void loadFileOrDirectory(File file) {
        if(file.isDirectory()) {
            for(File child : file.listFiles()) {
                loadFileOrDirectory(child);
            }
        }
        else {
            files.add(file);
        }
    }

    @Override
    public Iterator<DnaSequence> iterator() {

        return new SequenceIterator();
    }

    /**
     * Returns whether specified line in file is a sequence header
     * @param line some line loaded from a file
     * @return true if line is a header, false if not
     */
    private boolean isHeader(String line) {
        return line.length() > 0 && line.charAt(0) == '>';
    }

    /**
     * Loads animal name from a sequence header
     * @param header the header containing animal name
     * @return animal name
     */
    private String parseName(String header) {
        String[] chunks = header.split(" ");
        chunks[0] = "";
        return String.join(" ", chunks);
    }

    private class SequenceIterator implements Iterator<DnaSequence> {

        BufferedReader reader;
        DnaSequence sequence;
        String animalName; // Name of animal whose sequence is about to be parsed

        int filePointer = 0;

        protected SequenceIterator() {
            reader = null;
            sequence = null;
            animalName = null;
        }

        @Override
        public boolean hasNext() {
            if(sequence == null) {
                refreshSequence();
            }
            return sequence != null;
        }

        @Override
        public DnaSequence next() {
            if(sequence != null) {
                DnaSequence toReturn = sequence;
                sequence = null;
                return toReturn;
            }
            refreshSequence();
            return sequence;
        }

        /**
         * Refreshes sequence class object
         */
        private void refreshSequence() {
            try {
                if(reader == null || !reader.ready()) {
                    openNextFile();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            loadNextSequence();
            if(sequence == null) {
                openNextFile();
                try {
                    if(reader != null && reader.ready()) {
                        loadNextSequence();
                    }
                } catch (IOException e) {
                    // It's okay
                }
            }
        }


        /**
         * Opens new file and loads first sequence from it
         */
        private void openNextFile () {
            if(reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(filePointer < files.size()) {
                File file = files.get(filePointer++);
                System.out.println(file.getName());
                try {
                    reader = new BufferedReader(new FileReader(file));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                sequence = null;
                loadNextSequence(); // Initializes sequence name - sets name but doesn't set the content
            }
        }

        /**
         * Loads next DNA sequence and sets it as class variable
         */
        private void loadNextSequence() {
            String line;
            String sequenceContent = "";
            try {
                while((line = reader.readLine()) != null) {
                    if(isHeader(line)) {
                        if(sequenceContent.length() > 0) {
                            sequence = new DnaSequence(animalName, sequenceContent);
                        }
                        animalName = parseName(line);
                        break;
                    }
                    else {
                        sequenceContent += line;
                    }
                }
            } catch (FileNotFoundException e) {
                System.err.println(ERR_LOAD_SEQ_FILE);
                e.printStackTrace();
            } catch (IOException e) {
                System.err.println(ERR_LOAD_SEQ_FILE);
                e.printStackTrace();
            }
        }
    }
}
