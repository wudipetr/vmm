package edu.cvut.fit.kw.vmm.backend.sequences_comparement;

import edu.cvut.fit.kw.vmm.ScoringMatrix;
import edu.cvut.fit.kw.vmm.alignment_solution.AlignmentSolution;
import edu.cvut.fit.kw.vmm.alignment_solution.StringOperation;

import java.util.LinkedList;
import java.util.List;

public class NeedlemanWunsch extends AbstractSequenceComparator implements SequenceComparator {

    public NeedlemanWunsch(String str0, String str1) {
        super(str0, str1);
    }

    /**
     * Creates new sequence comparator using Needleman-Wunsch algorithm for global alignment with specified char
     * similarity score matrix
     * @param str0 first string to compare
     * @param str1 second string to compare
     * @param charSimilarity matrix specifying how much are chars similar
     */
    public NeedlemanWunsch(String str0, String str1, ScoringMatrix charSimilarity) {
        super(str0, str1, charSimilarity);
    }
    
    public AlignmentSolution solve() {
        initScoreMatrix();
        initDirectionMatrix();
        for(int y = 0; y < str0.length(); y++) {
            for(int x = 0; x < str1.length(); x++) {
                solveItem(y,x);
            }
        }
        return getAlignmentSolution();
    }

    /**
     * Computes item of the scores and stores it. It also stores how this value were achieved (which direction came the
     * value from) in direction matrix.
     * @param pos0 position of cahr in fist string
     * @param pos1 position of char in second string
     * @return score of the computed solution
     */

    protected int solveItem(int pos0, int pos1) {
        char char0 = str0.charAt(pos0);
        char char1 = str1.charAt(pos1);
        int scoreTop = scores[pos0][pos1 + 1] + DISTANCE_PENALTY;
        int scoreLeft = scores[pos0 + 1][pos1] + DISTANCE_PENALTY;
        int scoreDiagonal = scores[pos0][pos1] + getSimilarity(char0, char1);
        int bestScore = scoreLeft;
        TableDirection bestDirection = TableDirection.HORIZONTAL;
        if(scoreDiagonal >= scoreLeft && scoreDiagonal >= scoreTop) {
            bestScore = scoreDiagonal;
            bestDirection = TableDirection.DIAGONAL;
        }
        else if(scoreTop >= scoreDiagonal && scoreTop >= scoreLeft) {
            bestScore = scoreTop;
            bestDirection = TableDirection.VERTICAL;
        }

        scores[pos0 + 1][pos1 + 1] = bestScore;
        directions[pos0 + 1][pos1 + 1] = bestDirection;
        return bestScore;
    }

    protected void initScoreMatrix() {
        scores = new int[str0.length() + 1][str1.length() + 1];
        scores[0][0] = 0;
        for(int i = 1; i <= str0.length(); i++) {
            scores[i][0] = DISTANCE_PENALTY * i;
        }
        for(int i = 1; i <= str1.length(); i++) {
            scores[0][i] = DISTANCE_PENALTY * i;
        }
    }

    protected AlignmentSolution getAlignmentSolution() {
        int bestScore = scores[str0.length()][str1.length()];
        List<StringOperation> operations = getStringTransformations();
        return new AlignmentSolution(operations, bestScore);
    }

    /**
     * Returns list of operations which transform first string into the second one.
     * @return list of operations
     */
    private List<StringOperation> getStringTransformations() {
        List<StringOperation> operations = new LinkedList<>();
        int y = str0.length();
        int x = str1.length();
        while(y > 0 || x > 0) {
            NeedlemanWunsch.TableDirection direction = directions[y][x];
            switch (direction) {
                case DIAGONAL:
                    if(str0.charAt(y - 1) == str1.charAt(x - 1)) {
                        operations.add(0, StringOperation.COPY);
                    }
                    else {
                        operations.add(0, StringOperation.REPLACE);
                    }
                    y--;
                    x--;
                    break;
                case VERTICAL:
                    operations.add(0, StringOperation.DELETE);
                    y--;
                    break;
                case HORIZONTAL:
                    operations.add(0, StringOperation.ADD);
                    x--;
                    break;
            }
        }
        return operations;
    }

}
