package edu.cvut.fit.kw.vmm.alignment_solution;

import java.util.List;

/**
 * Represents solution of alignment of two strings
 */
public class AlignmentSolution implements Comparable<AlignmentSolution> {

    private static String TO_STRING_PREFIX = "Solution of alignment problem with similarity score of ";

    protected int similarity;
    protected List<StringOperation> operations;
    protected int str0Beginning;
    protected int str1Beginning;

    /**
     * Creates new way how to align two strings to have the common parts on the same position
     * @param operations operations which can be applied to first, second, ..., last character in the first string
     *                   (n-th operation in the list is applied to the n-th character in the string).
     *                   By applying all the operations, the first string is transformed into the second one.
     * @param similarity how similar the strings are (just a number - tells almost nothing but it's comparable to
     *                   another similarities)
     */
    public AlignmentSolution(List<StringOperation> operations, int similarity) {
        this(operations, similarity, 0,0);
    }

    /**
     * Creates new way how to align two strings to have the common parts on the same position
     * @param operations operations which can be applied to first, second, ..., last character in the first string
     *                   (n-th operation in the list is applied to the n-th character in the string).
     *                   By applying all the operations, the first string is transformed into the second one.
     * @param similarity how similar the strings are (just a number - tells almost nothing but it's comparable to
     *                   another similarities)
     * @param str0Beginning (in case of local alignment) beginning of the aligned part in first string
     * @param str1Beginning (in case of local alignment) beginning of the aligned part in second string
     */
    public AlignmentSolution(List<StringOperation> operations, int similarity, int str0Beginning, int str1Beginning) {
        this.operations = operations;
        this.similarity = similarity;
        this.str0Beginning = str0Beginning;
        this.str1Beginning = str1Beginning;
    }

    /**
     * @return list of operations who are necessary to change first string to the second one
     */
    public List<StringOperation> getOperations() {
        return operations;
    }

    /**
     * @return similarity of the two strings
     */
    public int getSimilarity() {
        return similarity;
    }

    /**
     * Beginning of the aligned part in the first string. In case of global alignment this is always equal 0.
     * @return position of first char which is part of the aligned sequence
     */
    public int getStr0Beginning() {
        return str0Beginning;
    }

    /**
     * Beginning of the aligned part in the second string. In case of global alignment this is always equal 0.
     * @return position of first char which is part of the aligned sequence
     */
    public int getStr1Beginning() {
        return str1Beginning;
    }

    @Override
    public String toString() {
        return TO_STRING_PREFIX + this.similarity;
    }

    @Override
    public int compareTo(AlignmentSolution alignmentSolution) {
        return Integer.compare(similarity, alignmentSolution.similarity);
    }
}
