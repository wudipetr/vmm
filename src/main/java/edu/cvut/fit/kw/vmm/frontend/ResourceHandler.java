package edu.cvut.fit.kw.vmm.frontend;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.IOException;
import java.io.OutputStream;

/**
 * HTTP handler. Decides what to do with request for home page.
 */
public class ResourceHandler implements HttpHandler {

    protected String path;

    public ResourceHandler(String path) {
        this.path = path;
    }

    public void handle(HttpExchange t) throws IOException {
        System.out.println("Request for resource: " + path);
        byte[] response = FileLoader.loadPage(path);
        t.sendResponseHeaders(200, response.length);
        OutputStream os = t.getResponseBody();
        os.write(response);
        os.close();
    }
}
