package edu.cvut.fit.kw.vmm.backend;

import edu.cvut.fit.kw.vmm.ScoringMatrix;
import edu.cvut.fit.kw.vmm.alignment_solution.AlignedSequencePair;

import java.util.List;

/**
 * Class containing methods for finding DNA similar to the specified one
 */
public interface SimilarDnaFinder {

    /**
     * Sets matrix of similarities between characters
     */
    void setScoringMatrix(ScoringMatrix scoringMatrix);

    /**
     * Finds k similar DNA sequences
     * @param sequence sequences similar to this will be found
     * @param alignment type of alignment (local/global)
     * @param k number of similar sequences to find
     * @return list of k dna sequence pairs - first sequence is always the one specified in input, second is the one
     * found in DNA collection
     */
    List<AlignedSequencePair> findKSimilar(String sequence, AlignmentType alignment, int k);


    /**
     * Finds DNA sequences with similarity to the given string higher than specified threshold
     * @param sequence sequences similar to this will be found
     * @param alignment type of alignment (local/global)
     * @param minSimilarity only sequences with value higher or equal this are included in result
     * @return list of most similar DNA sequences - first sequence is always the one specified in input, second is the
     * one found in DNA collection
     */
    List<AlignedSequencePair> findSimilarWithRange(String sequence, AlignmentType alignment, double minSimilarity);

}
