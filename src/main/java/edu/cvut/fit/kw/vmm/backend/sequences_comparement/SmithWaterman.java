package edu.cvut.fit.kw.vmm.backend.sequences_comparement;

import edu.cvut.fit.kw.vmm.ScoringMatrix;
import edu.cvut.fit.kw.vmm.alignment_solution.AlignmentSolution;
import edu.cvut.fit.kw.vmm.alignment_solution.StringOperation;

import java.util.LinkedList;
import java.util.List;

public class SmithWaterman extends AbstractSequenceComparator implements SequenceComparator {

    /**
     * Begginning of local aligned sequence on x part of the matrix
     */
    protected int xBeginning;

    /**
     * Begginning of local aligned sequence on y part of the matrix
     */
    protected int yBeginning;

	public SmithWaterman(String str0, String str1) {
		super(str0, str1);
	}

    public SmithWaterman(String str0, String str1, ScoringMatrix scoringMatrix) {
        super(str0, str1, scoringMatrix);
    }

	@Override
	protected void initScoreMatrix() {
        scores = new int[str0.length() + 1][str1.length() + 1];
        bBScore = new int[1][1];
        bBScore[0][0] = 0;
        scores[0][0] = 0;
        for(int i = 1; i <= str0.length(); i++) {
            scores[i][0] = 0;
        }
        for(int i = 1; i <= str1.length(); i++) {
            scores[0][i] = 0;
        }	
	}

    public AlignmentSolution solve() {
        initScoreMatrix();
        initDirectionMatrix();
        for(int y = 0; y < str0.length(); y++) {
            for(int x = 0; x < str1.length(); x++) {
                solveItem(y,x);
            }
        }
        return getAlignmentSolution();
    }

    protected int solveItem(int pos0, int pos1) {
        char char0 = str0.charAt(pos0);
        char char1 = str1.charAt(pos1);
        int scoreTop = scores[pos0][pos1 + 1] + DISTANCE_PENALTY;
        int scoreLeft = scores[pos0 + 1][pos1] + DISTANCE_PENALTY;
        int scoreDiagonal = scores[pos0][pos1] + getSimilarity(char0, char1);
        int bestScore = scoreLeft;
        TableDirection bestDirection = TableDirection.HORIZONTAL;
        if(scoreDiagonal >= scoreLeft && scoreDiagonal >= scoreTop) {
            bestScore = scoreDiagonal;
            bestDirection = TableDirection.DIAGONAL;
        }
        else if(scoreTop >= scoreDiagonal && scoreTop >= scoreLeft) {
            bestScore = scoreTop;
            bestDirection = TableDirection.VERTICAL;
        }
        if (bestScore < 0) {
            bestScore = 0;
            bestDirection = TableDirection.STOP;
        }

        scores[pos0 + 1][pos1 + 1] = bestScore;
        if (bestScore >= bBScore[0][0]) {
            bBScore[0][0] = bestScore;
            bestScoreX  = pos0 + 1;
            bestScoreY  = pos1 + 1;
        }
        directions[pos0 + 1][pos1 + 1] = bestDirection;
        return bestScore;
    }

	@Override
    protected AlignmentSolution getAlignmentSolution() {
        int bestScore = bBScore[0][0];
        List<StringOperation> operations = getStringTransformations();
        return new AlignmentSolution(operations, bestScore, yBeginning, xBeginning);
    }

	
    /**
     * Returns list of operations which transform first string into the second one.
     * @return list of operations
     */
    private List<StringOperation> getStringTransformations() {
        List<StringOperation> operations = new LinkedList<>();
        int y = bestScoreX;
        int x = bestScoreY;
        while(y > 0 && x > 0) {
            NeedlemanWunsch.TableDirection direction = directions[y][x];
            switch (direction) {
                case DIAGONAL:
                    if(str0.charAt(y - 1) == str1.charAt(x - 1)) {
                        operations.add(0, StringOperation.COPY);
                    }
                    else {
                        operations.add(0, StringOperation.REPLACE);
                    }
                    y--;
                    x--;
                    break;
                case VERTICAL:
                    operations.add(0, StringOperation.DELETE);
                    y--;
                    break;
                case HORIZONTAL:
                    operations.add(0, StringOperation.ADD);
                    x--;
                    break;
                case STOP:
                    xBeginning = x;
                    yBeginning = y;
                    return operations;
            }
        }
        xBeginning = x;
        yBeginning = y;
        return operations;
    }
}
